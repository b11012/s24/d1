// console.log("hi");

// ES6 Updates

// Exponent Operator (**)
const firstNum = 8 ** 2;
console.log(firstNum);
// result: 64

// before ES6 update
const secondNum = Math.pow(8, 2)
console.log(secondNum);
// result: 64

/*
	Mini-Activity: 3 mins
		>> Using exponent operator get the cube of 5
		>> Store the result in a variable called getCube
		>> Print the result in the console browser
		>> Send your outputs in Hangouts

*/
// S O L U T I O N

let getCube = 5 ** 3;
console.log(getCube)
// result: 125

// Template Literals
/*
	- Allows to write strings without using the concatenation operator (+)
	- Greatly helps with code readability
*/


let name = "Tine";
 
// before the update
// Uses single quotes ('')
let message = ('Hello ' + name + '! ' + 'Welcome to programming');
console.log('Result from pre-template literals:')
console.log(message);

// ES6 updates
// Strings Using Template Literal
// Uses backticks(``)
message = (`Hello ${name}! Welcome to programming.`)
console.log('Result with template literals:')
console.log(message);

let otherName = `John's`

/*
	Mini-Activity: 5 mins
		>> Create a sentence using the variables given below
		>> Send a screenshot output in Hangouts


*/

let string1 = "Zuitt";
let string2 = "Coding";
let string3 = "Bootcamp";
let string4 = "teaches";
let string5 = "Javascript";
let string6 = "as a";
let string7 = "programming language";

// S O L U T I O N
let sentence = `${string1} ${string2} ${string3} ${string4} ${string5} ${string6} ${string7}`
console.log(sentence);


/*
	- Template literals allow us to write strings with embedded JavaScript expressions
	- expressions are any valid unit of code that resolves to a value
	- "${}" are used to include JavaScript expressions in strings using template literals
*/
const interestRate = .1;
const principal = 1000;

console.log(`The interest in your savings account is: ${principal * interestRate}`);
// result: 100

// Array Destructuring
/*
	- Allows to unpack elements in arrays into distinct variables
	- Allows us to name array elements with variables instead of using index numbers
	- Helps with code readability
	Syntax:
		let / const [variableNameA, variableNameB, ...] = arrayName
*/
const fullName = ["Juan", "Dela", "Cruz"];

// before update
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`);

// ES6 updates
// note that order does matter in array destructuring
const [firstName, middleName, lastName] = fullName;

// assigning only to middle name
// const [, middleName, ] = fullName

console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`);

/*
	Mini-Activity: 5 mins
		>> Destructure the given array and save each element in the following variables

		pointGuard1 - Curry
		pointGuard2 - Lillard
		pointGuard3 - Paul
		pointGuard4 - Irving

		>> Print the 4 variables in the console
		>> Send your output in Hangouts

*/
let pointGuards = ['Curry', 'Lillard', 'Paul', 'Irving'];

// S O L U T I O N

const [pointGuard1, pointGuard2, pointGuard3, pointGuard4] = pointGuards;
console.log(pointGuard1);
console.log(pointGuard2);
console.log(pointGuard3);
console.log(pointGuard4);

// Object Destructure
/*
	- Allows to unpack properties of objects into distinct variables
	- Shortens the syntax for accessing properties from objects
	Syntax:
		let / const {propertyNameA, propertyNameB, ...} = objectName

*/
// before updates
const person = {
	givenName: 'Jane',
	maidenName: 'Dela',
	familyName: 'Cruz'
};

console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It was nice meeting you.`);

// ES6 Updates

const {givenName, maidenName, familyName} = person;

// note that order does NOT matter in object destructuring
// const {familyName, givenName, maidenName} = person;

// non-existing property in the object
// const {surname} = person;
// result: undefined


console.log(givenName);
console.log(maidenName);
console.log(familyName);
// console.log(surname);

console.log(`Hello ${givenName} ${maidenName} ${familyName}. It's nice meeting you again.`);

/*
	Mini-Activity
		>> Given the object below, destructure each property
		>> Create a sentence variable with the following string message:

			 My Pokemon is <pokemon1Name>, it is level <levelOfPokemon>. It is a <typeOfPokemon> type.

		>> Send outputs in Hangouts. 

*/

let pokemon1 = {
	characterName: "Charmander",
	level: 11,
	type: "Fire"
}

// S O L U T I O N

const {characterName, level, type} = pokemon1;

let sentence2 = `My Pokemon is ${characterName}, it is level ${level}. It is a ${type} type.`
console.log(sentence2);

// destructuring in function parameter
function getFullName({givenName, maidenName, familyName}){
	console.log(`${givenName} ${maidenName} ${familyName}`)
};

getFullName(person);

// Arrow Function
/*

	- Compact alternative syntax to traditional functions

	Syntax:
		const variableName = (parameter) => {
			statement / code block
		}
*/

// before update

// function printFullName(firstName, middleName, lastName){

// 	console.log(`${firstName} ${middleName} ${lastName}`)

// };

// printFullName('John', 'D', 'Smith');


// ES6 Update

const printFullName = (firstName, middleName, lastName) => {
	console.log(`${firstName} ${middleName} ${lastName}`);	
};

printFullName('John', 'D', 'Smith');

const students = ['John', 'Jane', 'Juan'];

// forEach with traditional function
// students.forEach(function(){

// });

// // // The function is only used in the "forEach" method to print out a text with the student's names
students.forEach(student => {
	console.log(`${student} is a student.`)
});

// forEach with implicit return
students.forEach(student => console.log(`${student} is a student.`));

// implicit return
	// - There are instances when you can omit the "return" statement
	// - This works because even without the "return" statement JavaScript implicitly adds it for the result of the function

// explicit return
// const add = (x, y) => {
// 	return x + y
// };

// let total = add(9, 18);
// console.log(total);
// result: 27

// with implicit return
// one-liner function
const add = (x, y) => x + y;

let total = add(27, 9);
console.log(total);
// result: 36

// Default function argument value
// Provides a default argument value if none is provided when the function is invoked

const greet = (name = 'User') => {
	return `Good day! ${name}.`
};

console.log(greet());

// traditional function

// function greet(name = 'User'){
// 	return `Good day! ${name}`
// };

// Class-Based Object Blueprints
/*

	- Allows creation/instantiation of objects using classes as blueprints

	- The constructor is a special method of a class for creating/initializing an object for that class.
	- The "this" keyword refers to the properties of an object created/initialized from the class
	- By using the "this" keyword and accessing an object's property, this allows us to reassign it's values
	
	Syntax:

		class className {
			constructor(objectPropertyA, objectPropertyB){
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
		}
*/

class Car{
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}


/*
	- The "new" operator creates/instantiates a new object with the given arguments as the values of it's properties

	Syntax:
	- let/const variableName = new ClassName();
*/

/*
	- Creating a constant with the "const" keyword and assigning it a value of an object makes it so we can't re-assign it with another data type
	- It does not mean that it's properties cannot be changed/immutable
*/
	// const myCar = new Car();

	let myCar = new Car();
	console.log(myCar);
	// result: undefined

	
	// Values of properties may be assigned after creation/instantiation of an object
	myCar.brand = 'Ford';
	myCar.name = 'Ranger Raptor';
	myCar.year = 2021;

	console.log(myCar);

	// create a new object from the car class with initialized values
	const newCar = new Car("Toyota", "Vios", 2021);
	console.log(newCar);
